package com.rousan.hrdemo.service;

import com.rousan.hrdemo.entity.Department;
import com.rousan.hrdemo.entity.Employee;
import com.rousan.hrdemo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;
    private  final DepartmentService departmentService;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, DepartmentService departmentService) {
        this.employeeRepository = employeeRepository;
        this.departmentService = departmentService;
    }

    public Employee createEmployee(Employee employee, Long departmentId) {
        Department department = departmentService.getDepartmentById(departmentId);
        employee.setDepartment(department);
        Employee created = employeeRepository.save(employee);
        return created;
    }

    public Employee updateEmployee(Employee employee, Long id) {
        Employee original = getEmployeeById(id);
        employee.setId(original.getId());
        employee.setDepartment(original.getDepartment());
        employee.setCreationDate(original.getCreationDate());
        employee = employeeRepository.save(employee);
        return employee;
    }

    public Employee getEmployeeById(Long id) {
        Optional<Employee> optionalEmployee = employeeRepository.findById(id);
        if (optionalEmployee.isPresent()) {
            return optionalEmployee.get();
        }
        throw new NoSuchElementException();
    }

    public Employee deleteEmployeeById(Long id) {
        Employee original = this.getEmployeeById(id);
        original.setDeleted(true);
        Employee deleted = employeeRepository.save(original);
        return deleted;
    }

    public List<Employee> getAllEmployees(Boolean active) {
        if (active) {
            return employeeRepository.findByActiveTrue();
        }
        return employeeRepository.findByActiveFalse();
    }

    public List<Employee> getEmployeesBy(Long departmentId, String firstName, String lastName) {
        List<Employee> employees = employeeRepository.findEmployeesByFirstNameAndLastNameAndDepartment(firstName, lastName, departmentId);
        return employees;
    }

    public List<Employee> getTopPaidEmployees() {
        List<Employee> employees = employeeRepository.findTop3ByOrderBySalaryDesc();
        return employees;
    }

    public Employee raiseEmployeeSalary(Long id, Float percentage) {
        if (percentage <= 0.0) {
            throw new IllegalArgumentException();
        }
        Employee original = getEmployeeById(id);
        float newSalary = original.getSalary() * percentage / 100 + original.getSalary();
        original.setSalary(newSalary);
        Employee updated = employeeRepository.save(original);
        return updated;
    }
}
