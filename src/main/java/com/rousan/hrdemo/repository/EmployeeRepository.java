package com.rousan.hrdemo.repository;

import com.rousan.hrdemo.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    List<Employee> findByActiveTrue();

    List<Employee> findByActiveFalse();

    @Query("SELECT e from Employees e WHERE " +
            "(:firstName is null or e.firstName = :firstName)" +
            "AND (:lastName is null or e.lastName = :lastName)" +
            "AND (:department is null or e.department.id = :department)")
    List<Employee> findEmployeesByFirstNameAndLastNameAndDepartment(
            String firstName, String lastName, Long department);

    List<Employee> findTop3ByOrderBySalaryDesc();
}