package com.rousan.hrdemo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rousan.hrdemo.entity.Employee;
import com.rousan.hrdemo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping(path = "department/{departmentId}/employee")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee, @PathVariable Long departmentId) {
        Employee created = employeeService.createEmployee(employee, departmentId);
        return new ResponseEntity<>(created, HttpStatus.CREATED);
    }

    @PatchMapping(path = "employee/{id}")
    public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee, @PathVariable Long id) {
        Employee updated = employeeService.updateEmployee(employee, id);
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }

    @GetMapping(path = "employee/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable Long id) {
        Employee employee = employeeService.getEmployeeById(id);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @GetMapping(path = "employeesBy")
    public ResponseEntity<List<Employee>> getEmployeesBy(@RequestParam(required = false) Long departmentId,
                                                        @RequestParam(required = false) String firstName,
                                                        @RequestParam(required = false) String lastName) {
        List<Employee> employees = employeeService.getEmployeesBy(departmentId,firstName,lastName);
        return new ResponseEntity<>(employees, HttpStatus.OK);

    }

    @GetMapping(path = "employees")
    public ResponseEntity<List<Employee>> getAllEmployees(@RequestParam Boolean active) {
        List<Employee> employees = employeeService.getAllEmployees(active);
        return new ResponseEntity<>(employees, HttpStatus.OK);

    }

    @DeleteMapping(path = "employee/{id}")
    public ResponseEntity<Employee> deleteEmployeeById(@PathVariable Long id) {
        Employee employee = employeeService.deleteEmployeeById(id);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @GetMapping(path = "employee/topPaid")
    public ResponseEntity<List<Employee>> getTopPaidEmployees() {
        List<Employee> employees = employeeService.getTopPaidEmployees();
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }

    @PatchMapping(path = "employee/{id}/salary")
    public ResponseEntity<Employee> raiseEmployeeSalary(@PathVariable Long id, @RequestParam Float percentage) {
        Employee updated = employeeService.raiseEmployeeSalary(id, percentage);
        return new ResponseEntity<>(updated, HttpStatus.OK);

    }


}
