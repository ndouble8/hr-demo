package com.rousan.hrdemo.controller;

import com.rousan.hrdemo.model.MyUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {

    private final JdbcUserDetailsManager jdbcUserDetailsManager;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserController(JdbcUserDetailsManager jdbcUserDetailsManager, BCryptPasswordEncoder passwordEncoder) {
        this.jdbcUserDetailsManager = jdbcUserDetailsManager;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping(value = "/register-user")
    public ResponseEntity<Void> register(@RequestBody MyUser myUser) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(myUser.getRoles()));
        String encodedPassword = passwordEncoder.encode(myUser.getPassword());
        User user = new User(myUser.getUserName(), encodedPassword, authorities);
        jdbcUserDetailsManager.createUser(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
