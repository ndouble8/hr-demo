-hr-demo is a project reflecting the requested notes in the shahid assignment
-The project is documented using swagger default documentation
-Database used is mysql, in light that the project data schema is structured, has relations and has a small scale of data
-there are four main tables 
1-authorities:-user roles
2-users: user data for authentication
3-departments: employees departments data
4-employees: employee information

-Authentication used is JDBC authentication

-The register endpoint is /register-user and a sample admin registeration is:

{
	"userName":"name",
	"password":"123456",
	"roles":"ROLE_ADMIN"
}

-All department/employee endpoints require a logged in user with the role ROLE_ADMIN

Requirements:
1) MySQL v8 running locally on default (3304) port, with username 'root' and password 'root'
2) JDK 14 was used as the SDK for the project.
3) Hibernate is set to automatically generate the schemas for the entities used (for the purpose of the assignment), except for the authentication schema which is initialized in the schema.sql file.


-All implemented :-
• APIs to create, delete and update employees.  
• List all active/ inactive employees.  
• API to search employees per department and/or by name.   
• API to raise employee salary (add ratio from current salary).   
(not implemented) • Upload employee CV as PDF or Word (Hint: you can use Amazon S3).  
• Return top 3 paid employees.  
• All endpoints need to be authenticated.   